new Vue({
    el: '#config',
    data: function () {
        var checkName = function (rule, value, callback) {
            var regex = new RegExp("^[a-zA-Z][a-zA-Z0-9_]{1,15}$");
            if (!regex.test(value)) {
                callback(new Error('2-16字以字母开头,允许使用字母/数字/下划线'));
            } else {
                callback();
            }
        };
        var checkInt = function (rule, value, callback) {
            var regex = /^\+?[1-9][0-9]*$/;
            if (!regex.test(value)) {
                callback(new Error('请输入整数'));
            } else {
                callback();
            }
        };
        return {
            addDialogVisible: false,
            versionVisible: false,
            curVesionEnabled: false,
            uploadDialogVisible: false,
            fileDialogVisible: false,
            options: [],
            files: [],
            selectData: [],
            addForm: {
                group: "",
                app: "",
                version: "0"
            },
            addRules: {
                group: [
                    {required: true, message: '请输入分组名称', trigger: 'blur'},
                    {validator: checkName, trigger: 'change'}
                ],
                app: [
                    {required: true, message: '请输入APP名称', trigger: 'blur'},
                    {validator: checkName, trigger: 'change'}
                ],
            },
            uploadUrl: base + "/admin/config/upload",
            fileList: [],
            uploadData: {},
            fileDialogTitle: "",
            fileDialogContent: "",
            fileDialogFilename: ""
        }
    },
    methods: {
        getGroup: function () {
            var self = this;
            $.post(base + "/admin/config/group_app_data", {}, function (data) {
                if (data.code == 0) {
                    self.options = data.data;
                }
            }, "json");
        },
        handleChange: function (obj) {
            this.selectData = obj;
            this.addForm.group = this.selectData[0];
            this.addForm.app = this.selectData[1];
            this.addForm.version = this.selectData[2];
            this.pageData()
        },
        pageData: function () {
            var self = this;
            var group = this.selectData[0];
            var app = this.selectData[1];
            var ver = this.selectData[2];
            self.curVesionEnabled = false;
            $.post(base + "/admin/config/files/" + group + "/" + app + "/" + ver, {}, function (data) {
                if (data.code == 0) {
                    self.files = data.data;
                    if (data.data == null || data.data.length == 0) {
                        self.$message({
                            message: "没有读到文件哦",
                            type: 'warning'
                        });
                    } else if (data.data.length > 0) {
                        self.curVesionEnabled = true;
                    }
                } else {
                    self.$message({
                        message: data.msg,
                        type: 'error'
                    });
                }
            }, "json");
        },
        openAdd: function () {
            this.addDialogVisible = true;
        },
        doAdd: function () {
            var self = this;
            self.$refs["addForm"].validate(function (valid) {
                if (valid) {
                    $.post(base + "/admin/config/create", self.addForm, function (data) {
                        if (data.code == 0) {
                            self.$message({
                                message: data.msg,
                                type: 'success'
                            });
                            self.addDialogVisible = false;
                            self.getGroup();
                        } else {
                            self.$message({
                                message: data.msg,
                                type: 'error'
                            });
                        }
                    }, "json");
                }
            });
        },
        queryGroup: function (queryString, cb) {
            cb(this.options);
        },
        queryApp: function (queryString, cb) {
            var apps = [];
            var self = this;
            $.each(self.options, function (i, o) {
                if (self.addForm.group == o.value) {
                    cb(o.children)
                }
            });
        },
        handleGroupSelect: function () {

        },
        handleAppSelect: function () {

        },
        doActive: function () {
            var self = this;
            if (self.selectData == null || self.selectData.length == 0) {
                self.$message({
                    message: "请先选择APP版本",
                    type: 'error'
                });
                return;
            }
            if (!self.curVesionEnabled) {
                self.$message({
                    message: "当前版本下没有配置文件",
                    type: 'error'
                });
                return;
            }
            var group = self.selectData[0];
            var app = self.selectData[1];
            var ver = self.selectData[2];
            self.$confirm('此操作将启用 ' + "/" + group + "/" + app + "/" + ver + " 版本", '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
                callback: function (a, b) {
                    if ("confirm" == a) {
                        $.post(base + "/admin/config/active/" + group + "/" + app + "/" + ver, {}, function (data) {
                            if (data.code == 0) {
                                self.$message({
                                    message: data.msg,
                                    type: 'success'
                                });
                                self.getGroup();
                            } else {
                                self.$message({
                                    message: data.msg,
                                    type: 'error'
                                });
                            }
                        }, "json");
                    }
                }
            });

        },
        doDelete: function () {
            var self = this;
            if (self.selectData == null || self.selectData.length == 0) {
                self.$message({
                    message: "请先选择APP版本",
                    type: 'error'
                });
                return;
            }
            var group = self.selectData[0];
            var app = self.selectData[1];
            var ver = self.selectData[2];
            self.$confirm('此操作将删除 ' + "/" + group + "/" + app + "/" + ver + " 版本", '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
                callback: function (a, b) {
                    if ("confirm" == a) {
                        $.post(base + "/admin/config/remove/" + group + "/" + app + "/" + ver, {}, function (data) {
                            if (data.code == 0) {
                                self.$message({
                                    message: data.msg,
                                    type: 'success'
                                });
                                self.getGroup();
                            } else {
                                self.$message({
                                    message: data.msg,
                                    type: 'error'
                                });
                            }
                        }, "json");
                    }
                }
            });
        },
        openUpload: function () {
            var group = this.selectData[0];
            var app = this.selectData[1];
            var ver = this.selectData[2];
            var self = this;
            if (self.selectData == null || self.selectData.length == 0) {
                self.$message({
                    message: "请先选择APP版本",
                    type: 'error'
                });
                return;
            }
            this.uploadData = {
                group: group,
                app: app,
                version: ver
            };
            this.uploadDialogVisible = true;
        },
        beforeUpload: function (file) {
            var s = file.name.substring(file.name.lastIndexOf(".") + 1);
            if ("txt" != s && "properties" != s && "ini" != s) {
                this.$message.warning("后缀名不正确");
                return false;
            }
            if (file.size / 1024 > 20) {
                this.$message.warning("文件不能大于20KB");
                return false;
            }
        },
        exceedUpload: function (files, fileList) {
            this.$message.warning("当前限制选择5个文件");
        },
        uploadSuccess: function () {
            this.$message.success("上传成功");
            this.pageData()
        },
        closeUpload: function () {
            this.fileList = [];
            this.uploadDialogVisible = false;
        },
        clearUpload: function () {
            this.fileList = [];
        },
        editFile: function (filename) {
            var self = this;
            var group = self.selectData[0];
            var app = self.selectData[1];
            var ver = self.selectData[2];
            self.fileDialogTitle = "编辑: /" + group + "/" + app + "/" + ver + "/" + filename;
            self.fileDialogContent = "";
            self.fileDialogFilename = filename;
            self.fileDialogVisible = true;
            $.post(base + "/admin/config/open/" + group + "/" + app + "/" + ver, {filename: filename}, function (data) {
                if (data.code == 0) {
                    self.fileDialogContent = data.data;
                } else {
                    self.$message({
                        message: data.msg,
                        type: 'error'
                    });
                }
            }, "json");
        },
        delFile: function (filename) {
            var self = this;
            var group = self.selectData[0];
            var app = self.selectData[1];
            var ver = self.selectData[2];
            self.$confirm('此操作将删除 ' + filename + " 文件", '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
                callback: function (a, b) {
                    if ("confirm" == a) {
                        $.post(base + "/admin/config/delete/" + group + "/" + app + "/" + ver, {filename: filename}, function (data) {
                            if (data.code == 0) {
                                self.$message({
                                    message: data.msg,
                                    type: 'success'
                                });
                                self.pageData();
                            } else {
                                self.$message({
                                    message: data.msg,
                                    type: 'error'
                                });
                            }
                        }, "json");
                    }
                }
            });
        },
        closeFile: function () {
            this.fileDialogVisible = false;
        },
        doSaveFile: function () {
            var self = this;
            var group = self.selectData[0];
            var app = self.selectData[1];
            var ver = self.selectData[2];
            $.post(base + "/admin/config/save/" + group + "/" + app + "/" + ver, {
                filename: self.fileDialogFilename,
                content: self.fileDialogContent
            }, function (data) {
                if (data.code == 0) {
                    self.$message({
                        message: data.msg,
                        type: 'success'
                    });
                } else {
                    self.$message({
                        message: data.msg,
                        type: 'error'
                    });
                }
            }, "json");
        }
    },
    created: function () {
        this.getGroup()
    }
})
;