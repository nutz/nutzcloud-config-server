new Vue({
    el: '#header',
    data: function () {
        var self=this;
        var validatePass = function (rule, value, callback) {
            if (value !== self.passForm.newPassword) {
                callback(new Error('两次输入密码不一致!'));
            } else {
                callback();
            }
        };
        return {
            dialogVisible:false,
            activeIndex:"config",
            passForm: {
                oldPassword: "",
                newPassword: "",
                passwordAgain: ""
            },
            passRules: {
                oldPassword: [
                    {required: true, message: '请输入旧密码', trigger: 'blur'}
                ],
                newPassword: [
                    {required: true, message: '请输入新密码', trigger: 'blur'},
                    {min: 3, max: 20, message: '密码长度必须是3-20位', trigger: 'change'}
                ],
                passwordAgain: [
                    {required: true, message: '请再次输入新密码', trigger: 'blur'},
                    {validator: validatePass, trigger: 'change'}
                ]
            },
        }
    },
    methods: {
        doLogout: function () {
            window.location.href = base + "/admin/user/logout";
        },
        doPass:function () {
            var self = this;
            self.$refs["passForm"].validate(function (valid) {
                if (valid) {
                    $.post(base+"/admin/user/pass", self.passForm, function (data) {
                        if (data.code == 0) {
                            self.$message({
                                message: data.msg,
                                type: 'success'
                            });
                            self.dialogVisible=false;
                            self.$refs["passForm"].resetFields();
                        } else {
                            self.$message({
                                message: data.msg,
                                type: 'error'
                            });
                        }
                    }, "json");
                }
            });
        }
    }
});