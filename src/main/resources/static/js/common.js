$.ajaxSetup({
    contentType: 'application/x-www-form-urlencoded;charset=utf-8',
    complete: function (XMLHttpRequest, textStatus) {
        var sessionstatus = XMLHttpRequest.getResponseHeader('loginStatus'); //通过XMLHttpRequest取得响应头，sessionstatus，
        if (sessionstatus == 'accessDenied') {
            window.location.href="/";
        }
        if (sessionstatus == 'unauthorized') {
            window.location.href="/";
        }
    }
});