package org.nutz.cloud.config.server.store;

import org.nutz.cloud.config.server.ConfigureStore;
import org.nutz.lang.Files;
import org.nutz.lang.Lang;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 将配置信息以文件方式存储
 *
 * @author wendal
 */
public class LocalFileConfigureStore implements ConfigureStore {

    protected File root;

    public LocalFileConfigureStore(File root) {
        this.root = root;
    }

    @Override
    public String get(String group, String app, int version, String filename) {
        File f = new File(root, group + "/" + app + "/" + version + "/" + filename);
        if (f.exists() && f.canRead())
            return Files.read(f);
        return null;
    }

    @Override
    public void save(String group, String app, int version, String filename, String content) {
        File f = new File(root, group + "/" + app + "/" + version + "/" + filename);
        Files.write(f, content);
    }

    @Override
    public void upload(String group, String app, int version, String filename, Object obj) {
        File f = new File(root, group + "/" + app + "/" + version + "/" + filename);
        Files.write(f, obj);
    }

    @Override
    public int[] versions(String group, String app) {
        File f = new File(root, group + "/" + app);
        if (f.exists() && f.isDirectory()) {
            String[] names = f.list();
            List<String> list = new ArrayList<>();
            for (String name : names) {
                if ("version".equals(name))
                    continue;
                list.add(name);
            }
            int[] re = new int[list.size()];
            int index = 0;
            for (String name : list) {
                try {
                    re[index] = Integer.parseInt(name);
                    index++;
                } catch (Throwable e) {
                }
            }
            //re = Arrays.copyOf(re, index + 1);
            Arrays.sort(re);
            return re;
        }
        return null;
    }

    @Override
    public String[] files(String group, String app, int version) {
        File f = new File(root, group + "/" + app + "/" + version);
        if (f.exists() && f.isDirectory())
            return f.list();
        return null;
    }

    @Override
    public boolean active(String group, String app, int version) {
        File f = new File(root, group + "/" + app);
        if (f.exists() && f.isDirectory()) {
            Files.write(new File(f, "version"), "" + version);
            return true;
        }
        return false;
    }

    @Override
    public int version(String group, String app) {
        File f = new File(root, group + "/" + app + "/version");
        if (f.exists())
            return Integer.parseInt(Files.read(f));
        return -1;
    }

    @Override
    public int clean(String group, String app) {
        // 暂不支持清理
        return 0;
    }

    @Override
    public void remove(String group, String app, int version) {
        File f = new File(root, group + "/" + app + "/" + version);
        if (f.exists() && f.isDirectory()) {
            Files.deleteDir(f);
        }
    }

    @Override
    public void remove(String group, String app, int version, String filename) {
        File f = new File(root, group + "/" + app + "/" + version + "/" + filename);
        if (f.exists()) {
            Files.deleteFile(f);
        }
    }

    @Override
    public int create(String group, String app, int baseVersion) {
        try {
            int[] versions = versions(group, app);
            int newVersion = 0;
            if (versions == null || versions.length == 0) {
                newVersion = 1;
            } else {
                newVersion = versions[versions.length - 1] + 1;
            }
            if (Files.isDirectory(new File(root, group + "/" + app + "/" + baseVersion))) {
                Files.copyDir(new File(root, group + "/" + app + "/" + baseVersion), new File(root, group + "/" + app + "/" + newVersion));
            } else {
                Files.createDirIfNoExists(new File(root, group + "/" + app + "/" + newVersion));
            }
            return newVersion;
        } catch (IOException e) {
            throw Lang.wrapThrow(e);
        }
    }

    @Override
    public void remove(String group, String app) {
        Files.deleteDir(new File(root, group + "/" + app));
    }

    @Override
    public void remove(String group) {
        Files.deleteDir(new File(root, group));
    }

    @Override
    public void create(String group, String app) {
        Files.createDirIfNoExists(new File(root, group + "/" + app));
    }

    @Override
    public void create(String group) {
        Files.createDirIfNoExists(new File(root, group));
    }

    @Override
    public String[] groups() {
        return root.list();
    }

    @Override
    public String[] apps(String group) {
        return new File(root, group).list();
    }
}
