package org.nutz.cloud.config.server;

public interface ConfigureStore {

    /**
     * 获取一个具体的配置文件
     *
     * @param group    所属的组
     * @param app      所属的app
     * @param version  版本号,可以是-1,即最新版
     * @param filename 文件名
     * @return 配置文件内容
     */
    String get(String group, String app, int version, String filename);

    /**
     * 存储的一个配置文件
     *
     * @param group    所属的组
     * @param app      所属的app
     * @param version  版本号
     * @param filename 文件名
     * @param content  配置文件内容
     */
    void save(String group, String app, int version, String filename, String content);

    /**
     * 存储的一个配置文件
     *
     * @param group    所属的组
     * @param app      所属的app
     * @param version  版本号
     * @param filename 文件名
     * @param obj      文件对象
     */
    void upload(String group, String app, int version, String filename, Object obj);

    /**
     * 获取指定组,指定app的配置文件版本号列表
     *
     * @param group
     * @param app
     * @return
     */
    int[] versions(String group, String app);

    /**
     * 获取指定组,指定app,指定版本号的, 配置文件名称列表
     *
     * @param group   所属的组
     * @param app     所属的app
     * @param version 版本号
     * @return 文件名列表
     */
    String[] files(String group, String app, int version);

    String[] groups();

    String[] apps(String group);

    /**
     * 激活配置信息, 同一个组,一个app,只会有一个版本号被激活
     */
    boolean active(String group, String app, int version);

    /**
     * 当前激活的版本号信息
     *
     * @param group 所属的组
     * @param app   所属的app
     * @return
     */
    int version(String group, String app);

    /**
     * 清理所有非当前激活的配置版本
     *
     * @param group 所属的组
     * @param app   所属的app
     */
    int clean(String group, String app);

    /**
     * 移除指定组,指定app,指定版本的配置信息
     *
     * @param group   所属的组
     * @param app     所属的app
     * @param version 指定版本号
     */
    void remove(String group, String app, int version);

    /**
     * 删除文件
     *
     * @param group    所属的组
     * @param app      所属的app
     * @param version  指定版本号
     * @param filename 文件名
     */
    void remove(String group, String app, int version, String filename);

    void remove(String group, String app);

    void remove(String group);

    int create(String group, String app, int baseVersion);

    void create(String group, String app);

    void create(String group);

}
