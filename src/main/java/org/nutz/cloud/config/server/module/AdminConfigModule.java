package org.nutz.cloud.config.server.module;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.nutz.cloud.config.server.ConfigureStore;
import org.nutz.cloud.config.server.base.Result;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.adaptor.WhaleAdaptor;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.impl.AdaptorErrorContext;
import org.nutz.mvc.upload.TempFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 配置本服务器
 *
 * @author wendal
 */
@IocBean
@At("/admin/config")
public class AdminConfigModule {

    @Inject
    protected ConfigureStore configureStore;
    @Inject
    protected Dao dao;

    @At("")
    @Ok("beetl:/admin/config/index.html")
    @RequiresAuthentication
    public void index(HttpServletRequest req) {

    }

    @At("/group_app_data")
    @Ok("json")
    @RequiresAuthentication
    public Object group_app_data(HttpServletRequest req) {
        try {
            List<NutMap> list = new ArrayList<>();
            String[] groups = configureStore.groups();
            if (groups != null && groups.length > 0) {
                for (String group : groups) {
                    NutMap map = NutMap.NEW().addv("value", group).addv("label", group);
                    String[] apps = configureStore.apps(group);
                    if (apps != null && apps.length > 0) {
                        List<NutMap> appList = new ArrayList<>();
                        for (String app : apps) {
                            NutMap appMap = NutMap.NEW().addv("value", app).addv("label", app);
                            int[] vers = configureStore.versions(group, app);
                            if (vers != null && vers.length > 0) {
                                List<NutMap> verList = new ArrayList<>();
                                int activeVer = configureStore.version(group, app);
                                for (int v : vers) {
                                    NutMap vMap = NutMap.NEW().addv("value", v);
                                    if (activeVer == v) {
                                        vMap.addv("label", v + " 启用中");
                                    } else {
                                        vMap.addv("label", v);
                                    }
                                    verList.add(vMap);
                                }
                                appMap.addv("children", verList);
                            }
                            appList.add(appMap);
                        }
                        map.addv("children", appList);
                    }
                    list.add(map);
                }
            }
            return Result.success(list);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/versions/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object versions(String group, String app) {
        return Result.success(configureStore.versions(group, app));
    }

    @At("/files/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object files(String group, String app, int version) {
        return Result.success(configureStore.files(group, app, version));
    }

    @At("/groups")
    @Ok("json")
    @RequiresAuthentication
    public Object groups() {
        return Result.success(configureStore.groups());
    }

    @At("/apps/?")
    @Ok("json")
    @RequiresAuthentication
    public Object apps(String group) {
        return Result.success(configureStore.apps(group));
    }

    @At("/create")
    @Ok("json")
    @RequiresAuthentication
    public Object create(@Param("group") String group, @Param("app") String app, @Param("version") int version) {
        try {
            return Result.success(configureStore.create(group, app, version));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/active/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object active(String group, String app, int version) {
        try {
            if (version == configureStore.version(group, app)) {
                return Result.success();
            }
            configureStore.active(group, app, version);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/remove/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object remove(String group, String app, int version) {
        try {
            if (version == configureStore.version(group, app)) {
                return Result.error("禁止删除启用中的版本");
            }
            configureStore.remove(group, app, version);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/upload")
    @Ok("json")
    @RequiresAuthentication
    @AdaptBy(type = WhaleAdaptor.class, args = {"ioc:fileUpload"})
    public Object upload(@Param("file") TempFile tf, @Param("group") String group, @Param("app") String app, @Param("version") int version, HttpServletRequest req, AdaptorErrorContext err) {
        try {
            if (err != null && err.getAdaptorErr() != null) {
                return Result.error("文件格式错误");
            } else if (tf == null) {
                return Result.error("空文件");
            } else {
                configureStore.upload(group, app, version, tf.getSubmittedFileName(), tf.getInputStream());
                return Result.success("上传成功");
            }
        } catch (Exception e) {
            return Result.error("系统错误");
        }
    }

    @At("/open/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object open(String group, String app, int version, @Param("filename") String filename) {
        try {
            return Result.success("", configureStore.get(group, app, version, filename));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/delete/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object delete(String group, String app, int version, @Param("filename") String filename) {
        try {
            configureStore.remove(group, app, version, filename);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/save/?/?/?")
    @Ok("json")
    @RequiresAuthentication
    public Object save(String group, String app, int version, @Param("filename") String filename, @Param("content") String content) {
        try {
            configureStore.save(group, app, version, filename, content);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

}
