package org.nutz.cloud.config.server.module;

import org.nutz.cloud.config.server.ConfigureStore;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.GET;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.view.HttpStatusView;

/**
 * 客户端API入口.
 * <p/>
 * <b>调用顺序: 先调用version入口获取版本号,然后读取需要使用的配置文件</b>
 * <p/>
 * @author wendal
 *
 */
@At("/api/v1/config/")
@IocBean(create = "init", depose = "depose")
@Ok("json:full")
public class ApiV1Module {

    @Inject
    protected PropertiesProxy conf;

    @Inject
    protected ConfigureStore configureStore;

    @Ok("raw")
    @GET
    @At("/?/?/version")
    public Object version(String group, String app) {
        int re = configureStore.version(group, app);
        if (re < 1)
            return HttpStatusView.HTTP_404;
        return re;
    }

    @Ok("raw")
    @GET
    @At("/?/?/?/**")
    public Object get(String group, String app, int version, String filename, @Param("key") String key) {
        String re = configureStore.get(group, app, version, filename);
        if (Strings.isBlank(re)) {
            return HttpStatusView.HTTP_404;
        }
        return re;
    }

    public void init() {}

    public void depose() {}

}
