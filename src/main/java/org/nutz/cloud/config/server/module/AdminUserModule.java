package org.nutz.cloud.config.server.module;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.nutz.cloud.config.server.base.Result;
import org.nutz.cloud.config.server.model.User;
import org.nutz.cloud.config.server.shiro.AuthenticationFilter;
import org.nutz.cloud.config.server.shiro.exception.CaptchaEmptyException;
import org.nutz.cloud.config.server.shiro.exception.CaptchaIncorrectException;
import org.nutz.cloud.config.server.util.Toolkit;
import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.img.Images;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.random.R;
import org.nutz.mvc.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;

/**
 * 配置本服务器
 *
 * @author wendal
 */
@IocBean
@At("/admin/user")
public class AdminUserModule {
    @Inject
    private Dao dao;

    @At("/login")
    @Ok("json")
    @Filters(@By(type = AuthenticationFilter.class))
    public Object login(@Attr("loginToken") AuthenticationToken token, HttpSession session) {
        try {
            Subject subject = SecurityUtils.getSubject();
            ThreadContext.bind(subject);
            subject.login(token);
            return Result.success("用户登录成功");
        } catch (CaptchaIncorrectException e) {
            return Result.error(4, "验证码不正确");
        } catch (CaptchaEmptyException e) {
            return Result.error(3, "请输入验证码");
        } catch (UnknownAccountException e) {
            return Result.error(1, "用户不存在");
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return Result.error(2, "用户名或密码不正确");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error();
        }
    }

    @At("/captcha")
    @Ok("raw:png")
    public BufferedImage captcha(@Param("w") int w, @Param("h") int h, HttpSession session) {
        if (w * h < 1) { //长或宽为0?重置为默认长宽.
            w = 145;
            h = 35;
        }
        String text = R.captchaNumber(4);
        session.setAttribute("captcha", text);
        return Images.createCaptcha(text, w, h, null, "FFF", null);
    }

    @At
    @Ok(">>:/")
    public void logout() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated())
            subject.logout();
    }

    @At("/pass")
    @Ok("json")
    @RequiresAuthentication
    public Object pass(@Param("oldPassword") String oldPassword, @Param("newPassword") String newPassword, HttpServletRequest req) {
        try {
            Subject subject = SecurityUtils.getSubject();
            User jvmUser = (User) subject.getPrincipal();
            User user = dao.fetch(User.class, jvmUser.getId());
            if (Toolkit.passwordCheck(oldPassword, user.getSalt(), user.getLoginpass())) {
                String salt = R.UU32();
                dao.update(User.class, Chain.make("salt", salt).add("loginpass", Toolkit.passwordEncode(newPassword, salt)), Cnd.where("id", "=", user.getId()));
                return Result.success("修改成功");
            } else {
                return Result.error("原密码不正确");
            }
        } catch (Exception e) {
            return Result.error();
        }
    }

    public void addUser() {
    }

    public void deleteUser() {
    }

    public void updateUser() {
    }

    public void queryUser() {
    }

    public void addRole() {
    }

    public void deleteRole() {
    }

    public void updateRole() {
    }

    public void queryRole() {
    }

    public void addPermission() {
    }

    public void deletePermission() {
    }

    public void updatePermission() {
    }

    public void queryPermission() {
    }
}
