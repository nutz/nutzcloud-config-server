package org.nutz.cloud.config.server.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.nutz.lang.Lang;

/**
 * Created by wizzer on 2018.09
 */
public class Toolkit {
    public static String passwordEncode(String password, String salt) {
        String str = salt + password + salt + "nutz";
        return Lang.digest("SHA-512", str);
    }

    public static boolean passwordCheck(String password, String salt, String dbPwd) {
        String str = salt + password + salt + "nutz";
        return Lang.digest("SHA-512", str).equals(dbPwd);
    }

    public static void doLogin(AuthenticationToken token, String userId) {
        Subject subject = SecurityUtils.getSubject();
        if (token != null)
            subject.login(token);
        subject.getSession().setAttribute("me", userId);
    }
}
