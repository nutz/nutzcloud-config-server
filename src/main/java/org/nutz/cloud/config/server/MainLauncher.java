package org.nutz.cloud.config.server;

import org.nutz.boot.NbApp;
import org.nutz.cloud.config.server.model.User;
import org.nutz.cloud.config.server.store.LocalFileConfigureStore;
import org.nutz.cloud.config.server.util.Toolkit;
import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.integration.shiro.ShiroSessionProvider;
import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Times;
import org.nutz.lang.random.R;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.SessionBy;

import java.io.File;

/**



 */
@Fail("http:500")
@IocBean(create = "init", depose = "depose")
@SessionBy(ShiroSessionProvider.class)
public class MainLauncher {

    @Inject
    protected PropertiesProxy conf;

    @Inject("refer:$ioc")
    protected Ioc ioc;
    @Inject
    protected Dao dao;


    @At("/")
    @Ok("beetl:/admin/index.html")
    public void index() {
    }


    public void init() {
        // 先固定用LocalFile实现
        String root = conf.get("config.server.localfile.dir", "/data/config_server/");
        ioc.addBean("configureStore", new LocalFileConfigureStore(new File(root)));
        Daos.createTablesInPackage(dao, "org.nutz.cloud.config.server", false);
        if (0 == dao.count(User.class)) {
            String salt = R.UU32();
            User user = new User();
            user.setLoginname("superadmin");
            user.setSalt(salt);
            user.setCreateAt(Times.getTS());
            user.setLoginpass(Toolkit.passwordEncode("1", salt));
            dao.insert(user);
        }
    }

    public void depose() {
    }

    public static void main(String[] args) throws Exception {
        new NbApp().setArgs(args).setPrintProcDoc(true).run();
    }

}
