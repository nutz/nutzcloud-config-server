package org.nutz.cloud.config.server.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.nutz.cloud.config.server.model.User;
import org.nutz.cloud.config.server.shiro.exception.CaptchaEmptyException;
import org.nutz.cloud.config.server.shiro.exception.CaptchaIncorrectException;
import org.nutz.cloud.config.server.util.Toolkit;
import org.nutz.dao.Cnd;
import org.nutz.integration.shiro.AbstractSimpleAuthorizingRealm;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;

/**
 * Created by wizzer on 2018.09
 */
@IocBean(name = "myRealm")
public class SimpleAuthorizingRealm extends AbstractSimpleAuthorizingRealm {

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // null usernames are invalid
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        String loginname = principals.getPrimaryPrincipal().toString();
        User user = dao().fetch(User.class, Cnd.where("loginname", "=", loginname));
        if (user == null)
            return null;
        SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();
        auth.addRole("admin");
        return auth;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        CaptchaToken captchaToken = (CaptchaToken) token;
        Session session = SecurityUtils.getSubject().getSession(true);
        if (Strings.isBlank(captchaToken.getUsername())) {
            throw Lang.makeThrow(AuthenticationException.class, "用户名不能为空");
        }
        int errCount = Integer.parseInt(Strings.sNull(session.getAttribute("errCount")));
        if (errCount > 2) {
            if (Strings.isBlank(captchaToken.getCaptcha())) {
                throw Lang.makeThrow(CaptchaEmptyException.class, "请输入验证码");
            }
            if (!captchaToken.getCaptcha().equalsIgnoreCase(Strings.sNull(session.getAttribute("captcha")))) {
                throw Lang.makeThrow(CaptchaIncorrectException.class, "验证码不正确");
            }
        }
        User user = dao().fetch(User.class, Cnd.where("loginname", "=", captchaToken.getUsername()));
        if (user == null) {
            session.setAttribute("errCount", errCount + 1);
            throw Lang.makeThrow(UnknownAccountException.class, "用户 [ %s ] 不存在", captchaToken.getUsername());
        }
        if (!Toolkit.passwordCheck(String.valueOf(captchaToken.getPassword()), user.getSalt(), user.getLoginpass())) {
            session.setAttribute("errCount", errCount + 1);
            throw Lang.makeThrow(AuthenticationException.class, "用户名或密码不正确");
        }
        session.setAttribute("errCount", 0);
        return new SimpleAccount(user, user.getLoginpass(), getName());
    }

    public SimpleAuthorizingRealm() {
        this(null, null);
    }

    public SimpleAuthorizingRealm(CacheManager cacheManager, CredentialsMatcher matcher) {
        super(cacheManager, matcher);
        setAuthenticationTokenClass(CaptchaToken.class);
    }

    public SimpleAuthorizingRealm(CacheManager cacheManager) {
        this(cacheManager, null);
    }

    public SimpleAuthorizingRealm(CredentialsMatcher matcher) {
        this(null, matcher);
    }
}
