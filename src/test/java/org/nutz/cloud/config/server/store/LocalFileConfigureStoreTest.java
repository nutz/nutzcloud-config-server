package org.nutz.cloud.config.server.store;

import java.io.File;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nutz.lang.Files;
import org.nutz.lang.random.R;
import org.nutz.lang.util.Disks;

public class LocalFileConfigureStoreTest extends Assert {
    
    protected LocalFileConfigureStore store;
    protected File root;

    @Before
    public void setUp() throws Exception {
        root = new File(Disks.normalize("~/tmp/config/" + R.UU32()));
        Files.createDirIfNoExists(root);
        store = new LocalFileConfigureStore(root);
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteDir(root);
    }

    @Test
    public void simple_test() {
        String group = "default";
        String app = "demo";
        // 创建组及app
        int version = store.create(group, app, 0);
        store.save(group, app, version, "application.properties", "server.port=9090");
        
        // 基本测试
        assertTrue(store.groups().length > 0);
        assertEquals("default", store.groups()[0]);
        assertTrue(store.apps("default").length > 0);
        assertEquals("demo", store.apps("default")[0]);
        
        // 读写测试
        assertEquals("server.port=9090", store.get(group, app, version, "application.properties"));
        int version2 = store.create(group, app, version);
        assertEquals("server.port=9090", store.get(group, app, version2, "application.properties"));
    }

}
