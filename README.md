# 简介

配置中心

## 基本设计思路

### 客户端与服务器端交互逻辑

```
客户端-->服务器  获取最新版本号 /api/v1/${group}/${app}/version
客户端<--服务器 返回版本号或者404
客户端-->服务器 获取配置文件内容 /api/v1/${group}/${app}/${version}/${filename}
```

保持通信过程的简洁性, 并兼容以Nginx/Apache/IIS直接对外服务

### 服务器端初始化的基本逻辑

1. 新建group    POST /admin/config/group
2. 新建app:     POST /admin/config/${group}/app
3. 新建version  POST /admin/config/${group}/${app}
4. 写入配置文件 POST /admin/config/${group}/${app}/${version}/${filename}
5. 激活配置     POST /admin/config/${group}/${app}/${version}/active

"新建version" 可以传入需要拷贝的版本号

## 安全性

* TODO 客户端与服务器交互时可选传输key,供鉴权用
* TODO 配置服务的管理权限体系

## 更多功能

* TODO 配置信息推送

## 如何启动

MainLauncher是入口,启动即可

## 环境要求

* 必须JDK8+
* eclipse或idea等IDE开发工具,可选

## 配置信息位置

数据库配置信息,jetty端口等配置信息,均位于src/main/resources/application.properties

## 命令下启动

仅供测试用,使用mvn命令即可

```
// for windows
set MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run

// for *uix
export MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run
```

## 相关资源

* 论坛: https://nutz.cn
* 官网: https://nutz.io
* 一键生成NB的项目: https://get.nutz.io
